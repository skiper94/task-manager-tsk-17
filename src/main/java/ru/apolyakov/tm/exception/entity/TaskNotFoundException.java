package ru.apolyakov.tm.exception.entity;

import ru.apolyakov.tm.exception.AbstractException;

public class TaskNotFoundException extends AbstractException {

    public TaskNotFoundException() {
        super("Error! Task not found!");
    }

}
