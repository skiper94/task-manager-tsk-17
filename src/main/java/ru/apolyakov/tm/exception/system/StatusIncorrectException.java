package ru.apolyakov.tm.exception.system;
import ru.apolyakov.tm.exception.AbstractException;

public class StatusIncorrectException extends AbstractException {

    public StatusIncorrectException(){
        super("Error! Status is incorrect!");
    }

}
