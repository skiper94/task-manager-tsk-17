package ru.apolyakov.tm.api.entity;

import ru.apolyakov.tm.api.enumerated.Status;

public interface IHasStatus {

    Status getStatus();

    void setStatus(Status status);

}
