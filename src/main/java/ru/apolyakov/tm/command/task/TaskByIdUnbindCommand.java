package ru.apolyakov.tm.command.task;

import ru.apolyakov.tm.exception.entity.TaskNotFoundException;
import ru.apolyakov.tm.model.Task;
import ru.apolyakov.tm.util.TerminalUtil;

public final class TaskByIdUnbindCommand extends AbstractTaskCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "task-unbind-by-id";
    }

    @Override
    public String description() {
        return "Unbind task by id";
    }

    @Override
    public void execute() {
        System.out.println("[DELETING TASK FROM PROJECT BY ID]");
        System.out.println("ENTER TASK ID:");
        System.out.println("to find the task id use the command: task-list");
        final String taskId = TerminalUtil.nextLine();
        final Task task = serviceLocator.getProjectTaskService().unbindTaskFromProject(taskId);
        if (task == null)  throw new TaskNotFoundException();
        else System.out.println("[TASK DELETED FROM PROJECT SUCCESSFUL]");
    }

}
