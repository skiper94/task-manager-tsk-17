package ru.apolyakov.tm.command.task;

import ru.apolyakov.tm.exception.entity.TaskNotFoundException;
import ru.apolyakov.tm.model.Task;
import ru.apolyakov.tm.util.TerminalUtil;
import ru.apolyakov.tm.api.enumerated.Status;

import java.util.Arrays;

public final class TaskByIndexSetStatusCommand extends AbstractTaskCommand{

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "task-set-status-by-index";
    }

    @Override
    public String description() {
        return "Set task status by index";
    }

    @Override
    public void execute() {
        System.out.println("[SET TASK STATUS]");
        System.out.println("ENTER INDEX:");
        final int index = TerminalUtil.nextNumber() - 1;
        System.out.println("ENTER NUMBER OF STATUS:");
        System.out.println("1: [NOT STARTED]");
        System.out.println("2: [IN PROGRESS]");
        System.out.println("3: [COMPLETE]");
        final Task task;
        final Integer statusNum = TerminalUtil.nextNumber();
        switch (statusNum) {
            case 1: task = serviceLocator.getTaskService().changeTaskStatusByIndex(index, Status.NOT_STARTED);;break;
            case 2: task = serviceLocator.getTaskService().changeTaskStatusByIndex(index, Status.IN_PROGRESS);break;
            case 3: task = serviceLocator.getTaskService().changeTaskStatusByIndex(index, Status.COMPLETE);break;
            default:
                task = null;
        }
        if (task == null) throw new TaskNotFoundException();
    }

}
