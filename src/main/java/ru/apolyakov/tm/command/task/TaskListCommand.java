package ru.apolyakov.tm.command.task;

import ru.apolyakov.tm.api.enumerated.Sort;
import ru.apolyakov.tm.model.Task;
import ru.apolyakov.tm.util.TerminalUtil;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public final class TaskListCommand extends AbstractTaskCommand{

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "task-list";
    }

    @Override
    public String description() {
        return "Show task list";
    }

    @Override
    public void execute() {
        System.out.println("[TASKS LIST]");
        System.out.println("ENTER SORT:");
        System.out.println(Arrays.toString(Sort.values()));
        final String sort = TerminalUtil.nextLine();
        List<Task> tasksSort = new ArrayList<>();
        if (sort == null || sort.isEmpty()) tasksSort = serviceLocator.getTaskService().findAll();
        else {
            final Sort sortType = Sort.valueOf(sort);
            System.out.println(sortType.getDispayName());
            tasksSort = serviceLocator.getTaskService().findAll(sortType.getComparator());
        }
        int index = 1;
        for (final Task task: tasksSort){
            System.out.println(index + ". " + task);
            index++;
        }
    }
}
