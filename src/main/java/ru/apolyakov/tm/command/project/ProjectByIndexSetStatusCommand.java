package ru.apolyakov.tm.command.project;

import ru.apolyakov.tm.api.enumerated.Status;
import ru.apolyakov.tm.exception.entity.ProjectNotFoundException;
import ru.apolyakov.tm.model.Project;
import ru.apolyakov.tm.util.TerminalUtil;

public final class ProjectByIndexSetStatusCommand extends AbstractProjectCommand{

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "project-set-status-by-index";
    }

    @Override
    public String description() {
        return "Set project status by index";
    }

    @Override
    public void execute() {
        System.out.println("[SETTING STATUS TO PROJECT BY INDEX]");
        System.out.println("ENTER PROJECT INDEX:");
        System.out.println("to find the project index use the command: project-list");
        final Integer projectIndex = TerminalUtil.nextNumber() -1;
        System.out.println("ENTER NUMBER OF STATUS:");
        System.out.println("1: [NOT STARTED]");
        System.out.println("2: [IN PROGRESS]");
        System.out.println("3: [COMPLETE]");
        final Project project;
        final Integer statusNum = TerminalUtil.nextNumber();
        switch (statusNum) {
            case 1: project = serviceLocator.getProjectService().changeProjectStatusByIndex(projectIndex, Status.NOT_STARTED);break;
            case 2: project = serviceLocator.getProjectService().changeProjectStatusByIndex(projectIndex, Status.IN_PROGRESS);break;
            case 3: project = serviceLocator.getProjectService().changeProjectStatusByIndex(projectIndex, Status.COMPLETE);break;
            default:
                project = null;
        }
        if (project == null) throw new ProjectNotFoundException();
    }

}
