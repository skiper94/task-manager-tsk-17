package ru.apolyakov.tm.command.project;

import ru.apolyakov.tm.exception.entity.ProjectNotFoundException;
import ru.apolyakov.tm.model.Project;
import ru.apolyakov.tm.util.TerminalUtil;

public final class ProjectByIndexUpdateCommand extends AbstractProjectCommand{

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "project-update-by-index";
    }

    @Override
    public String description() {
        return "Update project by index";
    }

    @Override
    public void execute() {
        System.out.println("[UPDATE PROJECT]");
        System.out.println("ENTER INDEX:");
        final Integer index = TerminalUtil.nextNumber() -1;
        final Project project = serviceLocator.getProjectService().findOneByIndex(index);
        if (project == null) throw new ProjectNotFoundException();
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION");
        final String description = TerminalUtil.nextLine();
        final Project projectUpdated = serviceLocator.getProjectService().updateProjectByIndex(index, name, description);
        if (projectUpdated == null) throw new ProjectNotFoundException();
    }

}
