package ru.apolyakov.tm.command.project;

import ru.apolyakov.tm.exception.entity.ProjectNotFoundException;
import ru.apolyakov.tm.model.Project;
import ru.apolyakov.tm.util.TerminalUtil;

public final class ProjectByNameUpdateCommand extends AbstractProjectCommand{

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "project-update-by-name";
    }

    @Override
    public String description() {
        return "Update project by name";
    }

    @Override
    public void execute() {
        System.out.println("[UPDATE PROJECT]");
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        final Project project = serviceLocator.getProjectService().findOneByName(name);
        if (project == null) throw new ProjectNotFoundException();
        System.out.println("ENTER NAME:");
        final String nameNew = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION");
        final String description = TerminalUtil.nextLine();
        final Project projectUpdated = serviceLocator.getProjectService().updateProjectByName(name, nameNew, description);
        if (projectUpdated == null) throw new ProjectNotFoundException();
    }

}
